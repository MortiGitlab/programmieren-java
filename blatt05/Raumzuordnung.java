package blatt05;


/** Beispiel f�r Referenzen: Klasse f�r Mitarbeiter-Raumzuordnung */
public class Raumzuordnung {
	private Raum raum;		// Referenz auf Raum
	private Person nutzer;	// Referenz auf Nutzer/in
	private String GrundDerZuordnung;
	// TODO Grund f�r Zuordnung (z.B. "B�roraum", "Labor") als Zeichenkette erg�nzen. 
	//		Darf nicht NULL sein und muss mindestens aus 3 Zeichen bestehen. 
	//		Bitte auch passende Zugriffsmethode und zus�tzlichen Konstruktor 
	//		mit Zuordnungsgrund hinzuf�gen.
	
	
	
	/** Konstruktor */
	public Raumzuordnung(Raum raum, Person nutzer ) {
		// TODO: pr�fen, dass jeweils nicht null
		this.raum = raum; this.nutzer = nutzer;
		if(raum!=null) {
			this.raum=raum;
			
		}
		
		else {
			System.out.println("Raum ist null");
			
		}
		if(nutzer!=null) {
			this.nutzer=nutzer;
			
		}
		
		else {
			System.out.println("nutzer ist null");
			
		}
		
		
		
	}
	
	
	
	
	
	
	
	
	
	/** Konstruktur mit GrundderZuordnung*/
	
	public Raumzuordnung(Raum raum, Person nutzer, String GrundDerZuordnung ) {
		
	
		this(raum,nutzer);// statt copy past 
		
		
		
		
		
		if(GrundDerZuordnung!=null && GrundDerZuordnung.length()>=3) {
			this.GrundDerZuordnung=GrundDerZuordnung;
		
		
		
	}
		else {
			
			System.out.println(" ohne Audio als B�ro ");
		}
		
		
		// 
		
		
		
		
	}
	
	
	
	
	public void print() {
		this.getNutzer().print();
		System.out.print(" nutzt den Raum: ");
		this.getRaum().print();
		System.out.println(GrundDerZuordnung);
		
	}
	
	Raum getRaum() {
		return this.raum;
	}
	
	Person getNutzer() {
		return this.nutzer;
	}
	
	public static void main(String[] a) { 
		// Br�ckl nutzt den Raum E 212a
		Person ub = new Person("Ulrich", "Br�ckl");
		Raum e212a = new Raum("e 212a", 2);
		String GrundDerZuordnung= " als B�ro ";
		Raumzuordnung ub2e212a = new Raumzuordnung(e212a, ub,GrundDerZuordnung);
		ub2e212a.print();	
		

		// Br�ckl nutzt au�erdem den Raum E 201
		Raumzuordnung ub2e201 = new Raumzuordnung(
				new Raum("e 201", 77, true), // keine Zwischenvariable f�r e201 n�tig...
				ub);
		// Den Raum dieser Zuordnung ausgeben:
		System.out.print("\n     Au�erdem: ");
		ub2e201.getRaum().print();
		System.out.println("\n");
		
		// Vogelsang nutzt den Raum E 209
		Raumzuordnung hv2e209 = new Raumzuordnung(
				new Raum("e 209", 3),
				new Person("Holger", "Vogelsang"),GrundDerZuordnung);
		hv2e209.print();
		
	}

}