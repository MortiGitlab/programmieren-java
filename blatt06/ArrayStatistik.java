package blatt06;

import java.util.Arrays;

public class ArrayStatistik {
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] liste= {35, 7, 90, 26, 92, 54, 24, 65, 2, 67, 43, 26, 80, 92, 7, 40, 67,
				66, 31, 45, 7, 100, 96, 93, 12, 20, 57, 22, 62, 51};
		int anzahl=0;
		int summe = 0;
		
		// summieren in einer schleife 
		for(int i=0;i<liste.length;i++) {
			summe=summe+liste[i];
		}
		// mittelwert berechnung
		double ergebniss=summe/liste.length;
		System.out.println("Arithmetischer Mittelwert: "+ergebniss);
		
		//Median (Tipp: Arrays.sort() sortiert ein Array aufsteigend)
		Arrays.sort(liste);
		System.out.println("Arrays.sort() sortiert ein Array aufsteigend: "+Arrays.toString(liste));

		double median;
		if(liste.length%2==0) {
			median = ((double)liste[liste.length/2] + (double)liste[liste.length/2 - 1])/2;
			
		}
		else {
			median = (double) liste[liste.length/2];
		}
		
		System.out.println("median ist : "+median);
		
		
		//Minimaler Wert und maximaler Wert
		//for 
		System.out.println("Minimaler Wert: "+ liste[0]);
		System.out.println("Maximaler Wert: "+ liste[liste.length-1]);
		
		
			
		
	}

}
