package blatt04;

import java.awt.*;
import javax.swing.JFrame;

/**
 * Klasse f�r das Zeichnen eines Sterns
 */
public class LoesungStern extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** Konstruktor. Nichts zu �ndern hier */
    public LoesungStern(){
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(600,400);
        this.setTitle("L�sung in der Klasse " + this.getClass().getName()); 
        this.setVisible(true);
    }

    /** Main-Methode. Nichts zu �ndern hier. */
    public static void main(String[] args){
    	new LoesungStern();
    }
   
    /** Paint-Methode. Nichts zu �ndern hier. Diese Methode 
     *  wird beim �ffnen des Fensters automatisch aufgerufen. 
     *  Die Methode wird auch aufgerufen, wenn die Gr��e des Fensters
     *  ver�ndert wird. 
     *  @param g Grafik, auf die gezeichnet wird.
     *  */
    public void paint(Graphics g){
    	/* L�schen des Fensterinhalts (wichtig bei erneutem Zeichnen): */
    	g.clearRect(0, 0, this.getWidth(), this.getHeight());
    	
    	
    	this.meinStern(g); // Aufruf der selbst programmierten Methode
    }
	/** 
	 * Hier bitte die L�sung, um den Stern zu zeichnen
	 * 
	 */
   public void meinStern(Graphics g){
	   // TODO
	   // Ursprung der Koordinaten einsetzen
	   
	   int x1 = this.getWidth() / 2;
	   int y1 = this.getHeight() / 2;
	   
	   
	   int n=8;
	   int r=150;
	   
	   
	   for(int i=0;i<(n-1);i++) {

			double phi = (i * (2 * Math.PI) / n);//berechnung Schwarze Linie bzw Punkt 
			double phi2 = (Math.PI) / n;// berechnung der rote linie bzw punkt
			
//Die schwarze Linie wird gezeichnet
			
			//die erste Punktkoordinate
			int x2 = ((int) (r * Math.cos(phi))) + x1;
			int y2 = ((int) (r * Math.sin(phi))) + y1;
			//die erste Punktkoordinate minus, da jeder Pnkt beim Kreis einen negativen Punkt hat 
			int x3 = ((int) -(r * Math.cos(phi))) + x1;
			int y3 = ((int) -(r * Math.sin(phi))) + y1;
			
//Die rote Linie wird hier gezeichnet
			
			
			int x4 = ((int) (r / 2 * Math.cos(phi + (phi2)))) + x1;
			int y4 = ((int) (r / 2 * Math.sin(phi + (phi2)))) + y1;
			int x5 = ((int) -(r / 2 * Math.cos(phi + (phi2)))) + x1;
			int y5 = ((int) -(r / 2 * Math.sin(phi + (phi2)))) + y1;

			
			g.setColor(Color.BLUE);
			g.drawLine(x2, y2, x3, y3);
			g.setColor(Color.RED);
			g.drawLine(x4, y4, x5, y5);
		   
		   
		   
		
		   
	   }
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
   } 
}