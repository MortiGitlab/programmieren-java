package blatt05;

import java.util.ArrayList;

public class test {
   public static void main(String[] args) {
      ArrayList<String> lines = new ArrayList<String>();
      
      lines.add("Drei Chinesen mit dem Kontrabass");
      lines.add("sa�en auf der Stra�e und erz�hlten sich was.");
      lines.add("Da kam ein Mann: ja was ist denn das?");
      lines.add("Drei Chinesen mit Kontrabass.");
      
      
      int max = 0;
      for(String line : lines){
         if(line.length()> max)
            max = line.length();
      }
      
      max+=10;
      
      for(String line: lines)
         printCentered(line, max);
         
   }

   
   public static void printCentered(String str, int width) {
      printChars(' ', (width - str.length()) / 2);
      System.out.println(str);
  }
   
   public static void printChars(char c, int len) {
      for (int i = 0; i < len; i++) {
          System.out.print(c);
      }
  }
}